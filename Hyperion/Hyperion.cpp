// Hyperion.cpp : Defines the entry point for the console application.
//
/*
*This is used to bridge the gap between subnets.
*It will act as beacon for the server/data source (i.e. X-Plane) to send the data over
*On start up it will send the broadcast message to the address defined in the Init.txt file
*to inform the server its IP address every one second.
*When the server the receive that message, the server will record the IP address and send back
*an acknowledge message before sending the data over.
*When received an acknowledge message, Hyperion will stop sending broadcast message. And will
*only listen for data.
*/

#include<winsock2.h>
#include <Ws2tcpip.h>
#include <fstream>
#include <string>
#include "stdafx.h"
#include <vector>
#include <iostream>
#include <tuple>
#include <chrono>
#pragma comment(lib,"ws2_32.lib")


int iResult;
WSADATA wsaData;
SOCKET dispatchSocket, receiveSocket, dispatchMulticast;
sockaddr_in broadcastAddress, receiveAddress, multicastAddress;
int broadcastAddressSize = sizeof(broadcastAddress);
int receiveAddressSize = sizeof(receiveAddress);
int multicastAddressSize = sizeof(multicastAddress);

struct broadcastMessage {
	//Type is just in case for the future where we have multiple servers on the same subnet
	//The type value will determine which server should answer the call
	//For now type will be 0 for the Boeing 737.
	int type;
	//This will be false when first sent out. If the server answers the call, it will mark
	//this as true. When it's true Hyperion will stop sending broadcast message.
	bool status;
};

broadcastMessage outMessage, inMessage;

struct primaryData {
	float Airspeed;
	float trueAirspeed;
	float SelectedAirspeed;
	float GroundSpeed;
	float Pitch;
	float Roll;
	float VerticalSpeed;
	float VerticalSpeedBug;
	float backupDisplayAltitude;
	float altitudeAboveGround;
	float SelectedAltitude;
	float Heading;
	float trueHeading;
	float SelectedHeading;
	float Latitude;
	float Longitude;
	float fuelLeftTank;
	float fuelRightTank;
	float fuelCenterTank;
	float N1Left;
	float N1Right;
	float N2Left;
	float N2Right;
	float EGTLeft;
	float EGTRight;
	float OilPressureLeft;
	float OilPressureRight;
	float OilTempLeft;
	float OilTempRight;
	float OilQuantityLeft;
	float OilQuantityRight;
	float FlapsHandle;
	float WindDirection;
	float Windspeed;
	int FlightDirectorStatus;
	float FlightDirectorPitch;
	float FlightDirectorRoll;
	float FuelFlowLeft;
	float FuelFlowRight;
	float GreenArcValue;
	int GreenArcStatus;
	float GreenArcDistance;
	float xtrack;
	float RadioAltimeter;
	int RadioAltimeterStep;
	float VNAV_ERR;
	int RunwayAltitude;
	int waypointAltitude[128];
	int zuluTime[3];
	int flapUpValue;
	int flapUpStatus;
	int flap1Value;
	int flap1Status;
	int flap5Value;
	int flap5Status;
	int flap15Value;
	int flap15Status;
	int flap25Value;
	int flap25Status;
	float maxSpeed;
	float minSpeed;
	int minSpeedStatus;
	int VORLineStatus;
	float VORLineCourse;
	float Course;
	int VOR1_Show;
	char VORName1[24];
	char VORName2[24];
	float VORDistance1;
	float VORDistance2;
	float runwayStart[2];
	float runwayEnd[2];
	float runwaycourse;
	float HdefPath;
	float NAVBearing;
	int DecisionHeight;
	int DecisionHeightStatus;
	int PullUpStatus;
	float Acceleration;
	float ILS_Horizontal_Value;
	float ILS_Vertical_Value;
	int ILS_Show;
	int ILS_Status;
	float VREFSpeed;
	int VREFStatus;
	int onGround;
	char scenario[5];
	float holdHeading1;
	float holdHeading2;
	float holdLegLength;
	float holdRadius;
	int holdStatus;
	float RNP;
	float ANP;
	float V1Speed;
	float V2Speed;
	float VRSpeed;
	float Mach;
	char Fix[5];
	int airspeedIsMach;
	int legs_alt_rest_type[128];
	float TOD;
	float throttleLevel1;
	float throttleLevel2;
	//Route data
	float latitudeArray[128];
	float longitudeArray[128];
	float latitudeArray2[128];
	float longitudeArray2[128];
	char waypointName[24];
	int legNumber;
	int legNumber2;
	float ETA;
	float waypointDistance;
	char legName[512];
	char legName2[512];
	char waypointNameWithType[512]; //List of waypoint that has to be display along with type
	int waypointType[20]; //Type of the waypoint and the route connect to it. 0 for none, 1 for non-active, 2 for active and 3 flyover, 4 for flyover active
	int legModActive; //Whether the route is being modified
	//Auto pilot data
	int autoThrottleStatus;
	int autoPilotSpeedMode;
	int autoPilotHeadingMode;
	int autopilotAltitudeMode;
	int autoPilotHeadingModeArm;
	int autoPilotAltitudeModeArm;
	int CMDAStatus;
	int CMDBStatus;
	//Data that needs to be either pilot or copilot
	//Pilot Side
	float AltitudePilot;
	int mapRangePilot;
	int airportStatusPilot;
	int waypointStatusPilot;
	int vor1StatusPilot;
	int vor2StatusPilot;
	int baroSTDStatusPilot;
	float baroInHgPilot;
	int baroUnitStatusPilot;
	int mapModePilot;
	int navaidStatusPilot;
	int pfdModePilot; //This is for determining whether it's ILS or LNAV/VNAV text
	int vertPathPilot; //This is for determining whether it's ILS or VOR
	char Text_Line1Pilot[10];
	char Text_Line2Pilot[10];
	int BaroBoxPilot;
	//Copilot Side
	float AltitudeCoPilot;
	int mapRangeCoPilot;
	int airportStatusCoPilot;
	int waypointStatusCoPilot;
	int vor1StatusCoPilot;
	int vor2StatusCoPilot;
	int baroSTDStatusCoPilot;
	float baroInHgCoPilot;
	int baroUnitStatusCoPilot;
	int mapModeCoPilot;
	int navaidStatusCoPilot;
	int pfdModeCoPilot;
	int vertPathCoPilot;
	char Text_Line1CoPilot[10];
	char Text_Line2CoPilot[10];
	int BaroBoxCoPilot;
};

primaryData forwardMessage, outDataMessage;

std::tuple<std::string, int> readInit(){
	std::ifstream file("HyperionInit.txt");
	std::tuple<std::string, int> returnValue;
	std::string str;
	if (!file.is_open()) {
		printf("Error while opening file\n");
		returnValue = std::make_tuple("NULL", 0);
	}
	else if (file.bad()) {
		printf("Error while reading file\n");
		returnValue = std::make_tuple("NULL", 0);
	}
	else {
		std::vector<std::string> stringVector;

		//Split the line
		while (std::getline(file, str))
		{
			std::string delimiter = ",";
			size_t pos = 0;
			std::string token;
			while ((pos = str.find(delimiter)) != std::string::npos) {
				token = str.substr(0, pos);
				stringVector.push_back(token);
				//printf("Token: %s|\n", token.c_str());
				str.erase(0, pos + delimiter.length());
			}
		}

		if (stringVector.size() != 0) {
			//If split succeeded
			//Make the return tuple from the splited string from the text file
			returnValue = std::make_tuple(stringVector.at(0), std::stoi(stringVector.at(1)));
		}
		else {
			returnValue = std::make_tuple("NULL", 0);
		}
		
	}
	return returnValue;
}

void setBroadcast(SOCKET inSocket, bool status){
	char setBroadcast;
	if (status) {
		setBroadcast = '1';
	}
	else {
		setBroadcast = '0';
	}
	//Make it a broadcast socket
	if (setsockopt(inSocket, SOL_SOCKET, SO_BROADCAST, &setBroadcast, sizeof(setBroadcast)) < 0)
	{
		std::cout << "Set socket broadcast option failed with error code:" << WSAGetLastError() << "\n";
	}
}

void setNonBlocking(SOCKET inSocket, bool status){
	u_long iMode; //1 for non-blocking, 0 for blocking

	if (status) {
		iMode = 1;
	}
	else {
		iMode = 0;
	}
	iResult = ioctlsocket(inSocket, FIONBIO, &iMode);
	if (iResult != NO_ERROR) {
		std::cout << "Make socket non-blocking failed with error: " << iResult << "\n";
	}

}

int startBroadcaster(){

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		std::cout << "WSAStartup for UDP failed with error " << iResult << "\n";
	}

	//Create dispatch socket
	if ((dispatchSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		std::cout << "dispatchSocket create failed with error code:" << WSAGetLastError() << "\n";
	}

	//Create multicast dispatch socket
	if ((dispatchMulticast = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		std::cout << "dispatchSocket create failed with error code:" << WSAGetLastError() << "\n";
	}

	setBroadcast(dispatchSocket, true);

	outMessage.type = 0;
	outMessage.status = false;

		//Set address structure
		broadcastAddress.sin_family = AF_INET;
		broadcastAddress.sin_port = htons(8888);
		InetPton(AF_INET, _T("192.168.74.255"), &broadcastAddress.sin_addr.s_addr);

		//Setup the socket to receive data from server
		//Create a receiver socket to receive datagrams
		receiveSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (receiveSocket == INVALID_SOCKET) {
			std::cout << "Create UDP socket for listening failed with error: " << WSAGetLastError() << "\n";
		}

		receiveAddress.sin_family = AF_INET;
		receiveAddress.sin_port = htons(8899);
		receiveAddress.sin_addr.s_addr = htonl(INADDR_ANY);

		// Bind the socket to any address and the specified port.
		iResult = bind(receiveSocket, (SOCKADDR *)& receiveAddress, sizeof (receiveAddress));
		if (iResult != 0) {
			std::cout << "receiveSocket bind failed with error: " << WSAGetLastError() << "\n";
		}

		//Make the socket non-blocking
		setNonBlocking(receiveSocket, true);
	



	//Set the clock
	std::chrono::system_clock::time_point currentTime;
	std::chrono::system_clock::time_point pastTime;
	std::chrono::seconds sec(1); //1 second in time
	currentTime = std::chrono::system_clock::now();
	pastTime = currentTime - sec;

	do {
		currentTime = std::chrono::system_clock::now();
		if (currentTime - pastTime > sec) {
			pastTime = currentTime;
			int result = sendto(dispatchSocket, (char*)&outMessage, sizeof(outMessage), 0, (SOCKADDR *)&broadcastAddress, broadcastAddressSize);
			if (result > 0) {
				std::cout << "Never fear for I am here." << "\n";
				//Listen for data come back from server
				recvfrom(receiveSocket, (char*)&inMessage, sizeof(inMessage), 0, (SOCKADDR *)& receiveAddress, &receiveAddressSize);
			}
			else {
				std::cout << "Sent fail: " << WSAGetLastError() << "\n";
			}
		}
	} while (!inMessage.status);

	//Now the server already has our IP address
	//We just need to read data and send it to the multicast address
	return 1;
}


int main() {
	int status = startBroadcaster();

	if (status) {

		//Clear all previous print out
		system("CLS");

		//Set address structure
		multicastAddress.sin_family = AF_INET;
		multicastAddress.sin_port = htons(8800);
		//The address 239.0.0.0 will be reserved for Boeing multicast group
		//Will need to create table for each system, ie 737, 777, helicopter
		//multicastAddress.sin_addr.S_un.S_addr = inet_addr("239.0.0.0");
		InetPton(AF_INET, _T("239.0.0.0"), &multicastAddress.sin_addr.s_addr);

		//Make the socket blocking again
		setNonBlocking(receiveSocket, false);

		//Revert broadcast option
		setBroadcast(dispatchSocket, false);

		while (1) {
			int result = recvfrom(receiveSocket, (char*)&forwardMessage, sizeof(forwardMessage), 0, (SOCKADDR *)& receiveAddress, &receiveAddressSize);
			if (result > 0) {
				std::cout << "Airspeed: " << forwardMessage.Airspeed << std::endl;
				int sendResult = sendto(dispatchMulticast, (char*)&forwardMessage, sizeof(forwardMessage), 0, (SOCKADDR *)&multicastAddress, multicastAddressSize);
				std::cout << "Sent fail: " << WSAGetLastError() << "\n";
			}
			else {
				std::cout << "REc fail: " << WSAGetLastError() << "\n";
			}
		}
	}
}